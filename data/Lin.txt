The draft of a paper to be published in the Gender and IT Encyclopedia (2006, Idea Groups)
A Techno-Feminist Perspective on the Free/Libre Open Source Software Development
Yuwei Lin (C) 2005
Department of Information Systems and Logistics
Vrije Universiteit Amsterdam
This paper is GFDL-ed. All rights reserved.
Abstract:
In this paper, I discuss the potential of free/libre open source software (FLOSS) in terms of
empowering women and minority users in computing compared with other proprietary
software, and how the current status of women and minority users in the FLOSS
development can be improved through engaging in the process of the FLOSS development.
Though taking a "techno-feminist" perspective (Wajcman 2004), I am not going to simplify the
gender issue in the FLOSS community to the level of a fight between men and women.
Staying away from such a reductionism, the issues I attempt to address include not only the
inequality that women face in computing, but also other inequalities that other users face
mainly emerging from the power relationships between expert and lay (namely, developer
and user) in software design. Instead of splitting women and men in the FLOSS
development, this analysis helps motivate both men and women to work together, reduce the
gender gap, and improve the disadvantaged statuses of women and a wider users
community in the FLOSS development. I also provide three examples on how both women
and men are encouraged to become mobile grassroots IT workers supporting organisations
and individuals with advises on technology with non-technical language, rather than just fixing
problems.
Author's Bio:
Yuwei Lin, Taiwanese, is a research fellow based at the Department of Information Systems,
Marketing and Logistics at the Vrije Universiteit Amsterdam. She received her PhD in
Sociology from the University of York (UK) in year 2004. Her PhD research investigated the
heterogeneity and contingency in the Free/Libre Open Source Software (FLOSS) social
worlds, which is based on a constellation of hacking practices, from the sociological
perspective. Her principal research interests centre on FLOSS studies, Science and
Technology Studies (STS), virtual communities and digital culture. She can be reached at
<ylin@feweb.vu.nl> or through her website at: <http://www.ylin.org>.
0. Forward
A hatred email titled 'Death to Women's Rights' appeared on the [Debian-women] mailing list
1The draft of a paper to be published in the Gender and IT Encyclopedia (2006, Idea Groups)
on 18 June 2005. In this email, a man said how much he despised women because women
usually complain there is no enough women in male-dominated but successful fields. This
certainly was not the first time that [Debian-women] was flamed by this kind of message.
Messages looking for female mates, messages discouraging women liberty/rights/freedom
movement appear often on the list even with filtering software or ban applied. This kind of
message demonstrates the unfriendly environment women usually face in the on-line
free/libre open source software (FLOSS) world, let alone the equally unfriendly environment
that women encounter off-line in the wider society (e.g. the forced marriages many Turkish
women faced as described in a story in Guardian on August 1, 20051). However, by taking
this hatred message as an example, I do not suggest that all men in the FLOSS world are
that ignorant. Instead of generalising the fact that women in computing and in FLOSS are
discriminated, hated and rejected by men, I have to say that there are also quite a few
sympathetic male developers constantly providing mutual help for their female counterparts.
In order not to fall into the gender stereotype between men and women when coming across
to the gender issue, I feel that we have to go a step beyond the number that just showing the
percentage of women participating in the FLOSS development or in the IT industry. We all
know that there are comparably few women involved in the FLOSS development or
computing. But why is this and what can we do with this number? The important thing is not
only to find out how to create a welcome environment for women to join the FLOSS
development, but also to come up with a better way of encouraging both sexes to collaborate
with each other.
To do so, we have to find out the reason for the problem and tackle it. I will start from how
FLOSS can make a difference for today's information society, and present some successful
stories of implementing FLOSS in developing countries and rural areas to empower women
and the minority. And then I will discuss the problem of including more women and the
minority in the FLOSS development through deconstructing the myth the programming skill.
1. How can FLOSS make a difference?
FLOSS allows users to run, copy, distribute, study, change and improve the software. The
essential element of FLOSS is 'freedom'. The cheaper cost of purchasing/acquiring (and the
very often misunderstood 'gratis') is just a side value. FLOSS is a matter of 'liberty', not 'price'.
Having source code made available to the public, interested users or developers can study
and understand how the software is written, and if competent, they can change and improve
it as well. In other words, apart from serving as an alternative choice for consumers, FLOSS
helps open up the black box of software technologies, facilitate the practice of participatory
design, and provide an opportunity of breaking down the hierarchy of professional
knowledge. And this could lead to improved security and usability because users can
1 http://books.guardian.co.uk/news/articles/0,6109,1540136,00.html
2The draft of a paper to be published in the Gender and IT Encyclopedia (2006, Idea Groups)
configure software to fulfill their local requirements, and secure against vandalism, user errors
and virus attacks.
Because of these opportunities, FLOSS has been adopted and implemented in several
developing countries and rural areas. Several European governments and companies have
also gradually switched from costly operating systems made by Microsoft and others to
FLOSS-based operating systems, like Linux (e.g. Munich, Barcelona, Bergen, Paris, and
Deutsch Bahn) (Blau 2004; IBNNews 2004; Kerner 2004; ZDNet UK 2004; McCarthy 2005).
Believing that FLOSS serves as better technological tools to bridge the digital divide, Brazil,
for example, has also required any company or research institute that receives government
financing to develop software to license their work under FLOSS licences, meaning the
underlying software code must be free to all (Benson 2005). In the wave of localisation and
customisation, a group of volunteers in India has started the IndLinux2 project to create a
Linux distribution that supports Indian Languages at all levels. The Simputer3, a low cost
portable alternative to PCs, has made the benefits of ICT reach the common people in India.
These examples are just a few out of the many ongoing projects around the world. They
demonstrate that FLOSS provides a better basis for more widespread access to ICT, more
effective uses and a much stronger platform for long-term growth and development compared
with scaled-down versions of proprietary software.
2. The myth of programming skill and knowledge
However, such FLOSS-based technologies meant to be used widely and to empower users
have not yet engaged with a diverse range of people in the development and implementation
yet. So far, the freedom of FLOSS seems to be enjoyed only by those who are capable of
manipulating the technologies. We see imbalanced population distributions in the FLOSS-
based knowledge demography, and the unbalanced gender distribution is amongst those top
ones. We see a strong programming culture in the FLOSS development and implementation
nowadays - if one does not programme, s/he seems to be left out of the FLOSS movement.
In other words, instead of breaking down the hierarchy of professional knowledge, a new
boundary and barrier of accessing ICT knowledge seems to be established. Abbreviations
such as 'RTFSC' (Read The Fucking4 Source Code) or 'RTFM' (Read The Fucking Manual)
shows how strong this hegemony of software knowledge is. This worship on programming
knowledge is the point I would like to strengthen here, which causes the gender inequality in
2 http://www.indlinux.org
3 http://www.simputer.org
4 To show politeness, the word 'fucking' now usually is replaced with the word 'fine' or hidden in the abbreviations. But
the meaning and the way it is used does not change.
3The draft of a paper to be published in the Gender and IT Encyclopedia (2006, Idea Groups)
FLOSS along with many other reasons (see my other contribution in this volume; Henson
2002).
To be involved in the FLOSS development, one needs not to be a programmer (see Rye
2004); one could write documentation, report or triage bugs, improve graphic or text content,
translate/localise, submit feature-requests, or teach how to use FLOSS. These activities are
equally important to programming in the software innovation process because software is not
ready to use just as it is written. It needs many efforts to make it user-friendly, implement it in
different contexts, and to maintain it overtime (Levesque 2004). To make FLOSS successful,
we need not only Richard Stallman or Linus Torvalds, but also a great amount of volunteers
reporting and fixing bugs, writing documentation, and more importantly, teach users how to
use OpenOffice.org and Mozilla Firefox browser. When thinking of an approach of including
more women and improving the representation of women in FLOSS, these activities can be
considered as essential.
Saying that we should start encouraging women to participate in these activities does not
imply that women are not good at programming. Not at all! There is no genuine biological
difference between men and women in programming. Given the history and the cultural and
educational backgrounds, perhaps many women nowadays may not have strong
programming experience, thus we need an alternative way of including women in FLOSS.
But more importantly, it is because neither are these activities (e.g. documentation and
localisation) subordinate to programming, nor are they peripheral in any case, we need to
encourage women and other minority user groups to participate in these activities in the
FLOSS development.
These efforts on documentation and localisation (including translation) are so important that
they are the keys to opening up the black box of the software technologies and allowing more
people (regardless of gender, class, race and disability) to participate in the FLOSS
development. While some people try to degrade the skill of writing documentation or
translation, an experienced female FLOSS user, Patricia Jung, emphasised the importance
and challenge of writing documentation on the Debian-women mailing list, that
Documentation can be a means of quality insurance, and this
power is far too seldom used, not only in Open Source
development. The people who write the best code I know write
documentation alongside or even before coding: The code has to
follow documentation, otherwise it's a bug :), at least
documentation and code are never allowed to get out of sync.
4The draft of a paper to be published in the Gender and IT Encyclopedia (2006, Idea Groups)
Which means documentation _is_ development, not just something
subordinate.
In a scenario like this documentation and usability are not just
nice-to-have but an inherent part of development and equally
important as writing code, and it finally leads you to better
software, to software that is aware of its users and tasks and
not just aware of how things are easiest, smartest to implement.
But it requires a paradigm shift: Coders are no longer allowed
to see documentation as a nasty add on, as something subordinate
and documentation people don't simply have to follow the
software they get but allowed and required to intervene.
Software isn't released as long as the doc people don't give
their go: Right, now code matches documentation, it does what it
is supposed to do, now we can release.
(http://lists.debian.org/debian-women/2005/05/msg00116.html)
Patricia's message demonstrates that coding is neither the only nor the foremost activity in
the FLOSS innovation process. Programmers do not play a more important role than other
contributors in the FLOSS development. The FLOSS community is comprised of diverse
people from different social worlds, and each member should gain equal respect from what
they do. FLOSS cannot get widespread without people writing documentation, reporting bugs
and mentoring. The value of the FLOSS development is embedded and embodied not only in
coding and the resulting code, but also in networking with others and the process of
collaboration. FLOSS gives us a chance to see the co-construction of social and technical
activities in a socio-technical innovation process. With a techno-feminist perspective
(Wajcman 2004) on the FLOSS development, the socio-technical complexity in the FLOSS
community can be observed more deliberately on their power relationships: the haves and
havenots in programming. Strengthening this gap would bring the problem to all members
involved (regardless of gender, race, class, and disability) in the FLOSS development, rather
than just men and women. And this would also clarify the misunderstanding on feminism:
feminism cares not only about the inequality between men, women and other genders, but all
inequalities in the society. In other words, involving more women in the documentation or
localisation (and internationalisation) of FLOSS should not turn these fields into a female
domain that it might ironically end up somehow cheapening the work suggested by some old
thoughts about certain things being women's work; instead, such a conflicting situation raises
the question on non-programming work in software design that is continuously undermined
and devalued. To mitigate this unbalance, we need to take a feminist perspective, treating
the whole mechanism of software design as a socio-technical system (Hughes 1979, 1987;
Latour 1983, 1999; Pinch & Bijker 1987), "recognising the various forms of visible and
5The draft of a paper to be published in the Gender and IT Encyclopedia (2006, Idea Groups)
invisible work that make up the production/use of technical systems, locating ourselves within
that extended web of connections, and taking responsibility for our participation" (Suchman
1999: 263). The biased power relationships between men and women, developers and users,
experts and lay inscribed in the strong technological determinism in the FLOSS development
can be overcome through valuing heterogeneity and situated knowledge in the FLOSS
community (Lin 2004).
3. Examples
As said, FLOSS has a potential of fostering an innovation environment that is not only friendly
to women but also to various minorities in our society. But to realise such a platform providing
both men and women, both expert and lay with equal opportunities to develop and implement
software together still requires more deliberate efforts. Many women-led FLOSS groups have
been tackling the knowledge gap between expert and lay (not only men and women) and
challenging the 'masculine' culture in the FLOSS community. I will introduce three groups
here.
LinuxChix
LinuxChix5 is a community for women who like Linux, and for supporting women in
computing. The membership ranges from novices to experienced users, and includes
professional and amateur programmers, system administrators and technical writers. It aims
at creating a more hospitable community in which people can discuss Linux, a community
that encourages participation, that doesn't allow the quieter among the members to be
drowned out by the vocal minority. LinuxChix was aimed at women, and it remains primarily a
group for supporting women in computing. LinuxChix now has several branches around the
world including LinuxChix Brazil6 and LinuxChix Africa7.
Debian-Women
The Debian-Women8 project was founded in May 2004. It seeks to balance and diversify the
Debian9 Project by actively engaging with interested women and encouraging them to
become more involved with Debian. Debian-women promotes women's involvement in
5
6
7
8
9
http://www.linuxchix.org
http://www.linuxchix.org.br/
http://www.africalinuxchix.org/
http://www.debianwomen.org
http://www.debian.org
6The draft of a paper to be published in the Gender and IT Encyclopedia (2006, Idea Groups)
Debian by increasing the visibility of active women, providing mentoring and role models, and
creating opportunities for collaboration with new and current members of the Debian Project.
All people (both men or women) who are interested in increasing the participation of women
in Debian are welcome. Now Debian-women has a mailing list running for discussion of
related issues, and an IRC channel for discussion of related issues, technical questions and
to allow women who are interested in contributing to Debian to meet each other and some of
Debian's current contributors. The members are also eager in giving talks at conferences,
and organising BOF discussions at Linux conferences, to promote discussion of issues facing
women and their involvement in Debian and Linux. These activities have effectively
encouraged and educated the Debian community to increase understanding of the specific
issues facing women who wish to contribute more to Debian.
Women’s Information Technology Transfer (WITT)
Women’s Information Technology Transfer (WITT)10 is a portal site to link women’s
organizations and feminist advocates for the internet in Eastern and Central Europe. It aims
at providing strategic ICT information to all, and supporting, in a collective way, Central and
Eastern European women in developing the web as an instrument in their social activism.
WITT is committed to bringing women’s actions, activities and struggles into the spotlight,
promoting the use of free software as a way to highlight women’s voices. The WITT website
has been developed for women to share their experiences with ICTs, to learn about training
events provided by WITT, and to develop expertise in advocacy on gender and ICT issues.
Women can publish on the website in their own language (eight languages are available to
be used as the site develops).
4. Conclusion
The features of FLOSS have been said to open up a range of opportunities to change the
power relationships in society: experts and lay, developers and users, developed and
developing countries, rich and poor etc. The feature of low development cost, modularised
features, transparent information are particularly celebrated in a knowledge-based society.
Whereas FLOSS is re/presented as a weapon to fight against proprietary software
companies such as Microsoft, neither have we seen an equal status for all members involved
in the FLOSS development, nor have we seen an accessible channel for all interested people
to enter this world. Drawing on new perspectives in feminist theory and science and
technology studies, I challenge the power emerging from the skills of programming and of
designing technologies that overlooks the requirements of having user-friendly technologies.
And it is exactly because of this misconception on the coding skill, it makes the composition
and structure in the FLOSS social world imbalanced. This misconception fosters a false
10 http://www.witt­project.net/
7The draft of a paper to be published in the Gender and IT Encyclopedia (2006, Idea Groups)
impression that FLOSS is too technical and difficult to use. This kind of misunderstanding
discourages many people, including women, to participate in the FLOSS development, and
subsequently results in an imbalance in gender, race, and class. Today, when we criticise the
women status in the FLOSS social world, we must not forget that a feminist critique not only
apply on gender issues, but it aims to challenge all kinds of power inequalities in the world. I
have proposed to look at the attitude in favour of (if not worshiping) people who own
programming skills when examining the reason why there are so few women in FLOSS.
Instead of narrowing the gender argument down to a fight between men and women, I argue
that this is not only about men and women, but about all majority and minority, the powerful
and the powerless class. In viewing the problem from a wider angle, we can overcome many
dilemmas such as if designing software for women is needed. After all, it is not whether
software should be designed for men or for women; it is whether the software is designed for
users without taking too much pride of the developers. I have introduced three women-led
FLOSS groups working persistently in this direction: LinuxChix, Debian-women and Women’s
Information Technology Transfer (WITT). These groups facilitate the networking and provide
mutual help amongst women participants in the FLOSS development. They help maintain a
pool of women who will not only promote ICT use but also promote a feminist approach of
design and usage of ICT. Although this paper has been focusing on the gender-related
issues specifically in the FLOSS development, the analytic concepts introduced here can and
should be widely applied to software design and other technological designs to explore the
ways in which technologies are gendered in their design and use.
References:
Benson, T. (2005). "BRAZIL: Free Software's Biggest and Best Friend". The New York
Times, 29 March, 2005.
Blau, J. (2004). "Munich Makes the Move to Linux". IDG News Service, 18 June, 2004. URL
(consulted on 18 June 2005) http://www.pcworld.com/news/article/0,aid,116568,00.asp
Henson, V. (2002). HOWTO Encourage Women in Linux. URL (consulted on 18 June 2005)
http://www.tldp.org/HOWTO/Encourage-Women-Linux-HOWTO/index.html
Hughes, Thomas P. (1979). "The Electrification of America: The System Builders".
Technology and Culture 20: 124-161.
Hughes, Thomas P. (1987). "The Evolution of Large Technological Systems". In The Social
Construction of Technological Systems. New Directions in the Sociology and History of
Technology. W.E. Bijker/T.P. Hughes/T.J. Pinch (eds.), pp. 51-82. Cambridge/Mass.: MIT
Press.
8The draft of a paper to be published in the Gender and IT Encyclopedia (2006, Idea Groups)
IBNNews. (2004). "Barcelona cambia al software libre", 21 July 2004. URL (consulted on 18
June 2005) http://iblnews.com/news/noticia.php3?id=111924
Kerner, S. M. (2004). "Big Strides For Civic Linux", Enterprise 17 June, 2004. URL (consulted
on 18 June 2005) http://internetnews.com/ent-news/article.php/3369931
Latour, Bruno. (1983). "Give Me a Laboratory and I will raise the World". In Science
Observed. Perspectives of the Social Studies of Science. K.D. Knorr-Cetina/M. Mulkay (eds.).
pp. 141-170. London: Sage.
Latour, B. (1999). Pandora's Hope: Essays on the Reality of Science Studies. Cambridge,
Mass.: Harvard University Press.
Levesque, M. (2004). "Fundamental issues with open source software development by
Michelle". First Monday, volume 9, number 4
URL: http://firstmonday.org/issues/issue9_4/levesque/index.html
Lin, Y.-W. 2004. Hacking Practices and Software Development: A Social Worlds Analysis of
ICT Innovation and the Role of Open Source Software. Department of Sociology, University
of York, UK. (Unpublished doctoral thesis).
McCarthy, K. (2005). "World's largest Linux migration gets major boost". PC Advisor, 03
February 2005. URL (consulted on 18 June 2005)
http://www.pcadvisor.co.uk/index.cfm?go=news.view&news=4514
Pinch, T. J. & Bijker, W. E. (1987). "The Social Construction of Facts and Artefacts: Or How
the Sociology of Science and the Sociology of Technology Might Benefit Each Other". In The
Social Construction of Technological Systems. New Directions in the Sociology and History
of Technology. W.E. Bijker & T.P. Hughes & T.J. Pinch (eds.). pp. 17-50. Cambridge/Mass.:
MIT Press.
Rye, J. B. 2004. I am not a programmer (IANAP - JBR Disclaimer). URL (consulted on 18
June 2005) http://www.xibalba.demon.co.uk/jbr/linux/ianap.html
Suchman, L. (1999). "Working relations of technology production and use" in D. Mackenzie &
J. Wajcman (eds) The social shaping of technology, 2nd edition. Maidenhead: Open
University Press.
Wajcman, J. (2004) Techno feminism. Polity.
ZDNet UK. (2004). 'Norway's second city embraces Linux' 15 June, 2004. URL (consulted on
18 June 2005) http://news.zdnet.co.uk/software/linuxunix/0,39020390,39157677,00.htm
9
