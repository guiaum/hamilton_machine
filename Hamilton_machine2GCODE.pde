/**
 *PENSER A REMTTRE LE HOMING POUR LA VERSION RELEASE
 *
 * Hamilton, machine à écrire avec une aiguille à coudre. 
 * 
 * Font used: https://datagoblin.itch.io/monogram
 * 1. Setting the font, drawing text and then convert it into dots
 * 7 pixels height for size 16  ?
 *
 * Processing ask for an action , Arduino send an D when its done
 * Actions asked:
 * E: 1 step X without hole
 * R: 1 step X with hole
 * T: 1 step -X without hole
 * Y: 1 step -X with hole
 * U: 1 step Y
 *
 * Arduino send a D when its done
 * a X when no paper
 
 
 */

import processing.serial.*;
import rita.*;

RiMarkov markov;
String[] files = { "Hamilton.txt", "Lin.txt" };

PFont f;
String s;
int margin = 0;

Serial myPort;                       // The serial port
boolean connectionWithGRBL = false;
int serialCount = 0;                 // A count of how many bytes we receive
int[] serialInArray = new int[3];    // Where we'll put what we receive


int state = 0;

// Where we are in the page
int currentPointX = 0;
int currentPointY = 0;
boolean backward = false;

boolean withArduino = true;
boolean debug=false;

//GCODE
String outputFolder = "./data/";
String outputFile = "bufferGCode";
PrintWriter output;
boolean GCodeExported = false;
int countGcode = 1;

// Déplacement en mm en X
int intervalX = 1;
// Déplacement en mm en Y
int intervalY = 1;

String penUp = "M3 S0";
String penDown = "M4";
boolean isPenDown = false; // used in order not to repeat unnecessarily the penDown command
// Define Feed rate (stepper motors speed)
float motorFeedSlow = 1000f;
float motorFeedFast = 1000f;

// Used for reading the generated GCode
String[] lines;
int index = 0;

boolean isStreamingGcode = false;
boolean firstContact = true;
char previousChar;

void setup() {
  size(150, 20);
  background(0);
  frameRate(1000);

  if (withArduino)
  {
    //Try to connect on each Serial port available, then wait for a response.
    //If nothing, no GRBL 
    printArray(Serial.list());
    for (int i=0; i<Serial.list().length; i++)
    {
      if (connectionWithGRBL == true) break;
      try {
        myPort = new Serial(this, Serial.list()[i], 115200);
      }
      catch(Exception e) {
        e.printStackTrace();
      }
      delay(2000);
      if (i == Serial.list().length-1 && connectionWithGRBL == false)
      {
        println("No GRBL found");
        break;
      }
    }
  }

  // create a markov model w' n=3 from the files
  markov = new RiMarkov(3, true, false);
  markov.loadFrom(files, this);

  // Create the font
  f = createFont("monogram_extended/monogram_extended.ttf", 16);
  textFont(f);
  textAlign(LEFT);
  s = "The quick brown fox jumped over the lazy dog.The quick brown fox jumped over the lazy dog.";
  fill(255);
  //noLoop();

  myPort.write("$X" + "\n");
  println("unlock");
  delay(1000);

  myPort.write("M3 S0" + "\n");
  println("needle up");
  delay(1000);

/*
  myPort.write("$H" + "\n");
  println("homing X");
  delay(10000);
  */
  myPort.write("G10 P0 L20 X0 Y0" + "\n");
  println("Define origin X");

} 

void draw() {

  switch (state)
  {
  case 0:
    if (withArduino)
    {
      // Waiting for arduino
      println("Waiting for arduino");
      if (firstContact)state = 10;
    } else {
      state = 10;
    }
    break;

  case 10:
    // Generating a page of text
    background(0);
    println("New Page");
    String[] lines = markov.generateSentences(10);
    //String[] lines = markov.generateTokens(100);
    s = RiTa.join(lines, " ");

    //flip text
    pushMatrix();
    translate(0, height);
    scale(1, -1);

    text(s, margin, margin, width-margin, height-margin);  // Text wraps within text box

    popMatrix();
    //////////////////////
      //GCODE
    output = createWriter(outputFolder + outputFile+".ngc");// for testing purpose
    GCodeInit();
    if (debug)
    {
      for (int i=0; i<height; i++)
      {
        for (int j=0; j<width; j++)
        {
          color c = get(j, i);
          if (c>-16777216)
          {
            print("X");
          } else {
            print(".");
          }
        }
        println();
      }
    }
    currentPointX = 0;
    currentPointY = 0;
    state = 20;
    break;

  case 20:
    // Ugly way to make recursion
    state = 30;
    break;


  case 30:
    // Check if line contains a hole. If not, one step Y.
    if (checkHolesInLine()==false)
    {
      //U: 1 step Y
      if (debug)println("no holes in line", currentPointY);
      if (debug)println("=> Arduino, one step Y", currentPointY);

      //GCODE
      GCode1LineDown(currentPointX, currentPointY);

      // check if bottom of page is not reached
      if (currentPointY < height -1)
      {
        currentPointY++;
        state = 20;
      } else {
        // Make a new page !
        //state = 10;
        GCodeEnd();
        GCodeExported = true;
        state = 60;
      }
    } else {
      // Make the line
      if (debug)println("holes in line", currentPointY);
      state = 40;
    }
    break;

  case 40:
    // Ugly way to make recursion, again
    state = 50;
    break;

  case 50:  
    // Read pixel
    color c = get(currentPointX, currentPointY);
    if (c>-16777216)
    {
      if (debug)println("=> Arduino, one step + make hole", currentPointX, currentPointY);
      stroke (0, 255, 0);
      //GCODE
      GCode1StepX(currentPointX, currentPointY);
      GCode1Dot();
    } else {
      if (debug)println("=> Arduino, one blank step", currentPointX, currentPointY);
      stroke (255, 0, 0);

      //GCODE
      //GCode1StepX(currentPointX, currentPointY);
    }
    point(currentPointX, currentPointY);
    // Check if we reached end of line
    if (backward == false)
    {
      if (currentPointX<width)
      {
        currentPointX++;
        state = 40;
      } else {
        // New line, backward
        //currentPointX = 0;
        backward = true;
        if (currentPointY < height -1)
        {
          currentPointY++;
          state = 20;
        } else {
          // Make a new page !
          state = 10;
        }
      }
    } else {
      if (currentPointX>0)
      {
        currentPointX--;
        state = 40;
      } else {
        backward = false;
        if (currentPointY < height -1)
        {
          currentPointY++;
          state = 20;
        } else {
          // Make a new page !
          state = 10;
        }
      }
    }
    //Send to Arduino some orders

    //Wait for response the next step

    break;


  case 60:
    //////////////////////////////////////////////////
    //Stream GCode
    //////////////////////////////////////////////////
    //fill (0, 125);
    //rect (0, 0, width, height);
    
    streamGCode();
    break;
  }


  //delay(5);
}
/*
void serialEvent(Serial myPort) {
 // read a byte from the serial port:
 int inByte = myPort.read();
 // if this is the first byte received, and it's an A,
 // clear the serial buffer and note that you've
 // had first contact from the microcontroller. 
 // Otherwise, add the incoming byte to the array:
 if (firstContact == false) {
 if (inByte == 'A') { 
 myPort.clear();          // clear the serial port buffer
 firstContact = true;     // you've had first contact from the microcontroller
 myPort.write('A');       // ask for more
 }
 } else {
 // Add the latest byte from the serial port to array:
 serialInArray[serialCount] = inByte;
 serialCount++;
 
 // If we have 3 bytes:
 if (serialCount > 2 ) {
 //xpos = serialInArray[0];
 //ypos = serialInArray[1];
 //fgcolor = serialInArray[2];
 
 // print the values (for debugging purposes only):
 //println(serialInArray[0] + "\t" + serialInArray[1] + "\t" + serialInArray[2]);
 
 // Send a capital A to request new sensor readings:
 myPort.write('A');
 // Reset serialCount:
 serialCount = 0;
 }
 }
 }
 */

void serialEvent(Serial myPort) {
  // read a char from the serial port:
  char inChar = myPort.readChar();
  // GRBL Connection
  if (inChar =='G' && connectionWithGRBL == false)
  {
    connectionWithGRBL = true;
    println ("Connection with GRBL is done");
  } 
  //received an "ok"
  else if (isStreamingGcode && inChar =='k' && previousChar=='o') {
    //if (index<lines.length) 
    if (index<50) 
    {
      String[] command = split(lines[index], '\n');
      String joinedCommand = join(command, " ");
      println();
      println ("Command is: " + joinedCommand + " --- Index is " + index + "/" +lines.length);
      myPort.write(joinedCommand + "\n");
      index++;
      if (index == 50)
      //if (index == lines.length)
      {

        println ("end of stream");
        delay(3000);
        //myPort.write(0x18 + "\n");
        isStreamingGcode=false;
        delay(2000);
        state = 10;
        //raz();
      }
    }
  } else {
    print (inChar);
  }
  previousChar = inChar;
}

boolean checkHolesInLine()
{
  boolean someHolesInTheLine = false;
  for (int i=0; i<width; i++)
  {
    color c = get(i, currentPointY);
    if (c>-16777216) someHolesInTheLine = true;
  }
  return someHolesInTheLine;
}


//GCODE
public void GCodeInit() {
  //We have to reset it only once, no idea where to put it elsewhere
  index = 0;
  
  System.out.println("Init");
  output.println("( Made with Processing / Paper size: "  + 240 + "x" + 280 + "mm )");
  output.println("G21");
  output.println(penUp);
  output.println("G0" + " " + "F" + motorFeedFast + " " + "X0.0"); 
  output.println(" ");
}

public void GCode1LineDown(int currentX, int endY) {
  //output.println("G21G91Y1F1000");
  output.println("G1X" + currentX*intervalX +"Y" + endY*intervalY);
}

public void GCode1StepX(int endX, int currentY) {
  //output.println("G0" + " " + "X" + endX*intervalX);
  output.println("G1X" + endX*intervalX + "Y" + currentY*intervalY);
}

public void GCode1Dot() {
  output.println(penDown);
  output.println("G4P0.3");
  output.println(penUp);
}

public void GCodeEnd() {
  System.out.println("End");
  // writes a footer with the end instructions for the GCode output file
  output.println(" ");
  // G0 Z90.0
  // G0 X0 Y0 => go home
  // M5 => stop spindle
  // M30 => stop execution
  output.println("G0 X0");  
  output.println("M3S0");
  //output.println("M30"); 
  // finalize the GCode text file and quits the current Processing Sketch
  output.flush();  // writes the remaining data to the file
  output.close();  // finishes the output file
  println("***************************");
  println("GCODE EXPORTED SUCCESSFULLY");
  println("***************************");
  delay(500);
}

void streamGCode()
{
  lines = loadStrings("bufferGCode.ngc");
  myPort.write(" " + "\n");
  delay(2000);
  isStreamingGcode = true;
}
